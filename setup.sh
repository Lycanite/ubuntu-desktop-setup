ver=$(lsb_release -sr);
codename=$(lsb_release -sc);

echo "========== Setting Up Repos =========="

echo "Adding Vivaldi Repo..."
wget -qO- http://repo.vivaldi.com/stable/linux_signing_key.pub | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=i386,amd64] http://repo.vivaldi.com/stable/deb/ stable main"

echo "Adding Dropbox Repo... (Bionic)"
sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E
sudo sh -c 'echo "deb http://linux.dropbox.com/ubuntu/ bionic main" >> /etc/apt/sources.list.d/dropbox.list'

echo "Adding Atom Repo..."
sudo add-apt-repository -y ppa:webupd8team/atom

echo "Adding IntelliJ Idea Repo..."
sudo add-apt-repository -y ppa:mmk2410/intellij-idea

echo "Adding OBS Repo..."
sudo add-apt-repository -y ppa:obsproject/obs-studio

echo "Adding WineHQ Repo..."
wget -nc https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ $codename main'

echo "Adding Lutris Repo..."
sudo add-apt-repository ppa:lutris-team/lutris

echo "Adding Radeon Profile Repo..."
sudo add-apt-repository -y ppa:obsproject/obs-studio

echo "Updating apt package lists..."
sudo apt update


echo "========== Installing Software =========="

echo "Installing wine (with recommends)..."
sudo apt install --install-recommends winehq-stable

echo "Installing packages..."
sudo apt install -y curl ssh fish git htop radeontop vivaldi-stable dropbox atom intellij-idea-community qtcreator qtdeclarative5-dev blender audacity krita obs-studio steam-installer steam-devices  lutris fonts-powerline slade python3-autopilot

echo "Installing Wine (with recommends)"
sudo apt install --install-recommends winehq-stable

echo "Installing GitAhead..."
wget https://github.com/gitahead/gitahead/releases/download/v2.6.1/GitAhead-2.6.1.sh
chmod +x GitAhead-2.6.1.sh
./GitAhead-2.6.1.sh
rm GitAhead-2.6.1.sh

echo "Installing Discord from deb download..."
wget -O discord.deb https://discordapp.com/api/download?platform=linux&format=deb
sudo yes | sudo dpkg -i discord.deb
rm discord.deb

echo "installing GZDoom from deb download"
wget -O gzdoom.deb https://forum.zdoom.org/viewforum.php?f=253
sudo yes | sudo dpkg -i gzdoom.deb
rm gzdoom.deb

echo "Running full upgrade..."
sudo apt full-upgrade


echo "========== Configuring Software =========="

echo "Setting up Fish..."
chsh -s /usr/bin/fish
sudo curl -L https://get.oh-my.fish | fish
omf install bobthefish

echo "Installing Atom Sync..."
apm install --no-confirm atom-package-sync

echo "Setting Up Steam Full Gamepad Support (uses python3-autopilot)"
sudo chmod 666 /dev/uinput
sudo udevadm trigger


echo "========== Configuring Plasma =========="
echo "Not Yet Implemented!"


echo "Desktop Setup Complete!";
